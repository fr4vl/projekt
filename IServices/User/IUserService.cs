﻿using System.Threading.Tasks;
using zakoppen.IServices.Requests;

namespace zakoppen.IServices.User
{
    public interface IUserService
    {
        Task<zakoppen.Domain.User.User> GetUserByUserNumericId (int numericId);
        Task<zakoppen.Domain.User.User> GetUserByNickname(string nickname);

        Task<zakoppen.Domain.User.User> CreateUser(CreateUser creayUser);
        Task<zakoppen.Domain.User.User> CreateUser(string creayUser);

        Task EditUser(EditUser editUser, int numericId);
    }
}
