﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zakoppen.Common.Enums;

namespace zakoppen.IServices.Requests
{
    public class CreateUser
    {
        public string UserId { get; set; }
        public int NumericUserId { get; set; }
    }
}
