﻿using System;
using zakoppen.Common.Enums;

namespace zakoppen.IServices.Requests
{
    public class EditUser
    {
        
        public string UserId { get; set; }
        public int NumericUserId { get; set; }
    }
}
