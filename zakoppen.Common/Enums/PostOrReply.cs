﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Common.Enums
{
    public enum PostOrReply
    {
        OriginalPost = 0,
        Reply = 1,
    }
}
