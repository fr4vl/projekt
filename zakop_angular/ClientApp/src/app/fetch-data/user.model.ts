export class UserViewModel {
  userId: string = '';
  userIdNumeric: number = 0;
}

export class UserViewModelFullString {
  userId: string = '';
  userIdNumeric: string = '0';
}
