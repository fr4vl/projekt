import { Component, Inject, Injectable } from '@angular/core';
import { UserViewModel ,UserViewModelFullString } from './user.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Component({
  selector: 'app-edit-data',
  templateUrl: './edit-data.component.html'
})
@
Injectable({
  providedIn: 'root'
})
export class EditDataComponent {
  //public users: UserViewModel[] = [];
  public user: UserViewModelFullString = new UserViewModelFullString;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.user.userId = 'NONEXISTENT';
    //this.http.get<UserViewModel>(this.baseURL + '/0')
    //  .subscribe(result => { this.user = result; });
  }
  readonly baseURL = 'https://localhost:5001/api/v2/User';


  public getUser(id: string) {
    this.http.get<UserViewModelFullString>(this.baseURL + '/' + id)
      .subscribe(result => { this.user = result; });
  }

  public SendPatches(id: string, nickname: string) {
    
    this.user.userId = nickname;
    this.user.userIdNumeric = id;
    this.http.patch<UserViewModelFullString>(this.baseURL + '/edit/' + id, this.user)
      .subscribe(result => { this.user = result; })
      ;
  }
  
}
