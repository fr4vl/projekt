import { Component, Inject, Injectable } from '@angular/core';
import {UserViewModel} from './user.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
@
Injectable({
  providedIn: 'root'
})
export class FetchDataComponent {
  public users: UserViewModel[] = [];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<UserViewModel[]>('https://localhost:5001/api/v2/user/allusers').subscribe(result => { this.users = result; })
  }

  //constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

  //}
  readonly baseURL = 'https://localhost:5001/api/v2/User';
  //formData: UserViewModel = new UserViewModel();


  public removeUser(user: UserViewModel) {
    this.http.delete<UserViewModel>(this.baseURL + '/delete/' + user.userIdNumeric)
      .subscribe(result => { user = result; }); 
  }
}
/*
export class FetchDataComponent {
public forecasts: UserViewModels[] =[];

//constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
//  http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
//    this.forecasts = result;
//  }, error => console.error(error));


constructor(http:  HttpClient, @Inject('BASE_URL') baseurl: string) {
  http.get<UserViewModels[]>('https://localhost:5001/api/v2/user/allusers').subscribe(result => {
    this.forecasts = result;
  }, error => console.error(error));

}
}


//export class PostDataComponent {
//  public forecasts: UserViewModels[] = [];

//  //constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
//  //  http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
//  //    this.forecasts = result;
//  //  }, error => console.error(error));
//  constructor(private httpClient: HttpClient) {
//  }
//  addUser(nickname: string): Observable<any> {
//    nickname:'nie';
//    return this.httpClient.post<string>('https://localhost:5001/api/v2/user', nickname).pipe();
//  }

//}




interface WeatherForecast {
date: string;
temperatureC: number;
temperatureF: number;
summary: string;
}

interface UserViewModels {
userId: string;
userIdNumeric: number;
}
*/
