import { Component, Inject, OnInit, Injectable } from '@angular/core';
import {UserViewModel} from './user.model';
import { HttpClient, HttpHeaders, HttpClientModule, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

//import UserViewModel = Usermodel.UserViewModel;


//import {BrowserModule } from '@angular/platform-browser';
@Component({
  selector: 'app-post-data',
  templateUrl: './post-data.component.html'

})
@
Injectable({
  providedIn: 'root'
})
export class PostDataComponent {
  //public forecasts: UserViewModels[] = [];
  //constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
  //  http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
  //    this.forecasts = result;
  //  }, error => console.error(error));

  /*http:HttpClient;*/

  user: UserViewModel = new UserViewModel();

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string)
  {
  //  this.http.get<UserViewModel>(this.baseURL + '/4').subscribe(result => {
  //  this.user = result;
  //});
  }
  readonly baseURL = 'https://localhost:5001/api/v2/User';

  userList: UserViewModel[] = [];
  
  str: string = '';

  

  public AddUser(nick: string) {
    this.user.userId = nick;
    this.user.userIdNumeric = 0;
    return this.http.post<UserViewModel>(this.baseURL, this.user).subscribe(result => {this.user = result});
  }
  

  
  //public sendValues() {
  //  this.http.post('http://localhost:5000/api/v2/user/nie','');
  //}

  //addUser(usr: string): Observable<string> {
  //  //return this.http.
  //}
}




//interface WeatherForecast {
//  date: string;
//  temperatureC: number;
//  temperatureF: number;
//  summary: string;
//}

interface UserViewModels {
  userId: string;
  userIdNumeric: number;
}
