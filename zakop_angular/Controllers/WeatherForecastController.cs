﻿using Microsoft.AspNetCore.Mvc;
//using zakoppen.API.Mappers;
using zakoppen.IServices;
//using zakoppen.IServices.User;

namespace zakop_angular.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }


        //private readonly IUserService _userService;
        //[HttpGet]
        //public async Task<IActionResult> GetUserById(int userId)
        //{
        //    var user = await _userService.GetUserByUserNumericId(userId);
        //    if (user.UserId != null)
        //    {
        //        return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
        //    }

        //    return NotFound("User not found");

        //}


    }
}