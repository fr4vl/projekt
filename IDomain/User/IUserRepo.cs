﻿using System.Threading.Tasks;

namespace zakoppen.IData.User
{
    public interface IUserRepo
    {
        Task<int> AddUser(zakoppen.Domain.User.User user);
        Task<zakoppen.Domain.User.User> GetUser(int userId);
        Task<zakoppen.Domain.User.User> GetUser(string userNickname);
        Task EditUser(Domain.User.User user);


    }
}