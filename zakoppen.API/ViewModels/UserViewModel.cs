using System;

namespace zakoppen.Api.ViewModels
{
    public class UserViewModel
    {
        public string  UserId { get; set; }
        public int UserIdNumeric { get; set; }

    }
}