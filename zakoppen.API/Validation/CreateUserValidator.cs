﻿using FluentValidation;
using zakoppen.API.BindingModels;

namespace zakoppen.API.Validation
{
    public class CreateUserValidator : AbstractValidator<CreateUser>
    {
        public CreateUserValidator()
        {
            RuleFor(x => x.UserId).NotNull();
        }
    }
}
