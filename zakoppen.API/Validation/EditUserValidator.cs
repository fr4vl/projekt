﻿using FluentValidation;
using zakoppen.API.BindingModels;

namespace zakoppen.API.Validation
{
    public class EditUserValidator : AbstractValidator<EditUser>
    {
        public EditUserValidator()
        {
            RuleFor(x => x.UserId).NotNull();
        }
    }
}
