﻿using zakoppen.Api.ViewModels;

namespace zakoppen.API.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(Domain.User.User user)
        {
            var userViewModel = new UserViewModel
            {
                UserId = user.UserId,
                UserIdNumeric = user.NumericId
            };
            return userViewModel;
        }
    }
}
