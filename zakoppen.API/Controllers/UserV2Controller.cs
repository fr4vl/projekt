using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using zakoppen.Api.Validation;
using zakoppen.Data.Sql;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using zakoppen.API.Mappers;
using zakoppen.Api.ViewModels;
using zakoppen.Data.Sql.DAO;
using zakoppen.IServices.User;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace zakoppen.API.Controllers
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/User")]

    public class UserV2Controller : Controller
    {
        private readonly zakoppenDbContext _context;
        private readonly IUserService _userService;

        public UserV2Controller(zakoppenDbContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _userService.GetUserByUserNumericId(userId);
            if (user.UserId != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }

            return NotFound("User not found");

        }

        [HttpGet("nick/{userName}", Name = "GetUserByNickname")]
        public async Task<IActionResult> GetUserByUsermame(string username)
        {
            var user = await _userService.GetUserByNickname(username);

            if (user.UserId != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));

            }

            return NotFound("User not found");

        }



        public async Task<IActionResult> Post([FromBody] IServices.Requests.CreateUser createUser)
        {
            var user = await _userService.CreateUser(createUser);

            return Created(user.UserId, UserToUserViewModelMapper.UserToUserViewModel(user));
        }

        [HttpPost("{username}")]

        public async Task<IActionResult> Post(string username)
        {
            var user = await _userService.CreateUser(username);

            return Created(user.UserId, UserToUserViewModelMapper.UserToUserViewModel(user));
        }


        [HttpDelete("delete/{userId:min(1)}", Name = "DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {

            var media = _context.FileHandle.Where(x => x.SubPost.User.NumericUserId == userId);
            //  Declared in two parts
            //  Media from user's replies (subposts) is removed first
            if (media != null)
            {
                _context.FileHandle.RemoveRange(media);
                await _context.SaveChangesAsync();
            }

            if (media == null) return StatusCode(418);

            media = _context.FileHandle.Where(x => x.Post.User.NumericUserId == userId);
            if (media != null)
            {
                _context.FileHandle.RemoveRange(media);
                await _context.SaveChangesAsync();
            }

            if (media == null) return StatusCode(418);


            var subpost = _context.SubPost.Where(x => x.AuthorUserNumeric == userId);
            if (subpost != null)
            {
                _context.SubPost.RemoveRange(subpost);
                await _context.SaveChangesAsync();
            }

            if (subpost == null) return StatusCode(418);

            var post = _context.Post.Where(x => x.AuthorUserNumeric == userId);
            if (post != null)
            {
                _context.Post.RemoveRange(post);
                await _context.SaveChangesAsync();
            }

            //var usercred = await _context.UserCreds.FirstOrDefaultAsync(x => x.NumericUserId == userId);
            //_context.UserCreds.Remove(usercred);
            //await _context.SaveChangesAsync();

            if (post == null) return StatusCode(418);
            var user = await _context.User.FirstOrDefaultAsync(x => x.NumericUserId == userId);
            _context.User.Remove(user);
            //_context.SaveChanges();
            await _context.SaveChangesAsync();
            return StatusCode(418);
            return NoContent();
            return Ok(new UserViewModel
            {
                UserId = user.UserId,
            });
        }

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
        public async Task<IActionResult> EditUser([FromBody] IServices.Requests.EditUser editUser, int userId)
        {
            editUser.NumericUserId = userId;
            await _userService.EditUser(editUser, userId);

            return NoContent();
        }

        [AllowAnonymous]
        [HttpGet("ConfirmNumericId/{userId:min(1)}", Name = "ConfirmNumericId")]
        public async Task<IActionResult> ConfirmNumericId(int userId)
        {
            var user = await _userService.GetUserByUserNumericId(userId);
            if (user != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }

            return NotFound();
        }

        [AllowAnonymous]
        [HttpGet("ConfirmNickname/{nickname}", Name = "ConfirmNickname")]
        public async Task<IActionResult> ConfirmNickname(string nickname)
        {
            var user = await _userService.GetUserByNickname(nickname);
            if (user != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }

            return NotFound();
        }


        [AllowAnonymous]
        [HttpGet("AllUsers", Name = "GetAllUsers")]
        public async Task<IActionResult> getAllUsers()
        {
            var users = _context.User.Where(x => x.NumericUserId > 0);

            var toret = new List<UserViewModel>();

            UserViewModel k = new UserViewModel();
            foreach (var local in users)
            {
                zakoppen.Domain.User.User no = new zakoppen.Domain.User.User(local.NumericUserId, local.UserId);

                toret.Add(UserToUserViewModelMapper.UserToUserViewModel(no));
                k = UserToUserViewModelMapper.UserToUserViewModel(no);
            }

            UserViewModel[] trueToRet = toret.ToArray();
            return Ok(trueToRet);


        }

    }
}