using System;
using System.Linq;
using System.Threading.Tasks;
using zakoppen.API.BindingModels;
using zakoppen.Api.Validation;
using zakoppen.Api.ViewModels;
using zakoppen.Data.Sql;
using zakoppen.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace zakoppen.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class UserController : Controller
    {
        private readonly zakoppenDbContext _context;

        public UserController(zakoppenDbContext context)
        {
            _context = context;
        }

        [HttpGet("nick/{userId}", Name = "GetUserByNick")]
        public async Task<IActionResult> GetUserByNick(string userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserIdNumeric = user.NumericUserId,
                    UserId = user.UserId
                });
            }

            return NotFound();
        }

        [HttpGet("{userId:min(1)}", Name = "GetUserByNumeric")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.NumericUserId == userId);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserIdNumeric = user.NumericUserId,
                    UserId = user.UserId
                });
            }

            return NotFound();
        }




        [ValidateModel]
//        [Consumes("application/x-www-form-urlencoded")]
//        [HttpPost("create", Name = "CreateUser")]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = new User
            {
                UserId = createUser.UserId,
            };
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return Created(user.UserId.ToString(), new UserViewModel
            {
                UserId = user.UserId,
                UserIdNumeric = ((_context.User.Max(x => x.NumericUserId)))
            });
        }

        [HttpDelete("delete/{userId:min(1)}", Name = "DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.NumericUserId == userId);
            _context.User.Remove(user);
            //_context.SaveChanges();
            await _context.SaveChangesAsync();
            return NoContent();
            return Ok(new UserViewModel
            {
                UserId = user.UserId,
            });
        }
        

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
        //        public async Task<IActionResult> EditUser([FromBody] EditUser editUser,[FromQuery] int userId)
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.NumericUserId == userId);
            user.UserId = editUser.UserId;
            user.NumericUserId = userId;
            await _context.SaveChangesAsync();
            return NoContent();
            return Ok(new UserViewModel
            {
                UserId = user.UserId,
            });
        }
    }
}