using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using zakoppen.Data.Sql;
using zakoppen.Data.Sql.Migrations;
using zakoppen.API.BindingModels;
using zakoppen.API.Middlewares;
using zakoppen.API.HealthChecks;
using zakoppen.API.Validation;
using zakoppen.Data.Sql.User;
using zakoppen.IData.User;
using zakoppen.IServices.User;
using zakoppen.Services.User;


namespace zakoppen.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private const string MySqlHealthCheckName = "mysql";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();


            services.AddRazorPages();
            services.AddDbContext<zakoppenDbContext>(optionsAction: options => options
                .UseMySQL(Configuration.GetConnectionString(name: "zakoppenDbContext")));




            services.AddTransient<DatabaseSeed>();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }).AddFluentValidation();

            services.AddTransient<IValidator<EditUser>,
                EditUserValidator>();
            services.AddTransient<IValidator<CreateUser>,
                CreateUserValidator>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepo, UserRepo>();

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.UseApiBehavior = false;
            });
            services.AddHealthChecks()
                .AddMySql(
                    Configuration.GetConnectionString("zakoppenDbServer"),
                    4,
                    10,
                    MySqlHealthCheckName);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                //.WithOrigins("https://localhost:44496")); // Allow only this origin can also have multiple origins seperated with comma
                .SetIsOriginAllowed(origin => true)); // Allow any origin  


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<zakoppenDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                databaseSeed.Seed();

            }

            app.UseAuthentication();
            app.UseAuthorization();
            // app.UseMvcWithDefaultRoute();

            app.UseMiddleware<ErrorHandlerMiddleware>();
            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });


        }

    }
}
