using System;
using System.ComponentModel.DataAnnotations;

namespace zakoppen.API.BindingModels
{
    public class CreateUser
    {
        [Required]

        [Display(Name = "UserNumericId")]

        public string UserId { get; set; }

        [Display(Name = "UserId")]

        public string NumericUserId { get; set; }

    }
}