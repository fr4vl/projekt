using System;
using System.ComponentModel.DataAnnotations;
using FluentValidation;

namespace zakoppen.API.BindingModels
{
    public class EditUser
    {
        //        [Required]
        [Display(Name = "UserId")]

        public string UserId { get; set; }
        [Display(Name = "UserNumericId")]

        public int UserIdNumeric { get; set; }
    }
}