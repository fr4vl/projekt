﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Domain.DomainExceptions
{
    public class InvalidUserIdException : Exception
    {

        public InvalidUserIdException(string userid) : base(ModifyMessage(userid))
        {

        }

        private static string ModifyMessage(string uid)
        {
            return $"Invalid user ID: {uid}";
        }
    }
}
