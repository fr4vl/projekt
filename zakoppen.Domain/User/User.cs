﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Domain.User
{
    public class User
    {
        public int NumericId { get; set; }
        public string UserId { get; set; }


        public User(int id, string username)
        {
            NumericId = id;
            UserId = username;
        }
        public User(string username)
        {
            UserId = username;
        }

        public User(int uid)
        {
            UserId = UserId;
            NumericId = uid;
        }
        public void EditUser(int uid, string userName)
        {
            NumericId = uid;
            UserId = userName;
        }

    }
}
