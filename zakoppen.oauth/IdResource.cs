﻿using System.Collections;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using zakoppen.Api.Validation;
using zakoppen.Data.Sql;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using zakoppen.API.Mappers;
using zakoppen.Api.ViewModels;
using zakoppen.Data.Sql.DAO;
using zakoppen.IServices.User;
using Microsoft.AspNetCore.Authentication.OAuth;
using IdentityServer4;
using IdentityServer4.Models;

namespace zakoppen.OAuth
{
    public class IdResource
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
//              new IdentityResources.OpenId()
                new IdentityResource(
                    name: "account",
                    userClaims: new [] {"name", "passphrase"},
                    displayName: "Test data"
                )
            };
        }
        
    }
}