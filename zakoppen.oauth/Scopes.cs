﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityServer4.Models;

namespace zakoppen.OAuth
{
    public class Scopes
    {
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope(name: "delete", displayName: "Delete your data."),
                new ApiScope(name: "read",   displayName: "Read public data."),
                new ApiScope(name: "writePost",  displayName: "Submit posts."),
                new ApiScope(name: "writeSubpost",  displayName: "Submit replies (subposts)."),
                new ApiScope(name: "writePostAttachment",  displayName: "Submit attachments to your posts."),
                new ApiScope(name: "writeSubPostAttachment",  displayName: "Submit attachments to your replies (subposts)."),
            };
        }
    }
}
