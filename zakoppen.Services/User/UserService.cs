﻿using System.Threading.Tasks;
using zakoppen.IData.User;
using zakoppen.IServices.Requests;
using zakoppen.IServices.User;

namespace zakoppen.Services.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepo _userRepo;

        public UserService(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        public Task<Domain.User.User> GetUserByUserNumericId(int userId)
        {
            return _userRepo.GetUser(userId);
        }

        public Task<Domain.User.User> GetUserByNickname(string userNickname)
        {
            return _userRepo.GetUser(userNickname);
        }

        public async Task<Domain.User.User> CreateUser(CreateUser createUser)
        {
            var user = new Domain.User.User(createUser.UserId);
            user.NumericId = await _userRepo.AddUser(user);
            return user;
        }
        public async Task<Domain.User.User> CreateUser(string createUser)
        {
            var user = new Domain.User.User(createUser);
            user.NumericId = await _userRepo.AddUser(user);
            return user;
        }
        public async Task EditUser(EditUser editUser, int userId)
        {
            var user = await _userRepo.GetUser(userId);
            user.EditUser(editUser.NumericUserId, editUser.UserId);
            await _userRepo.EditUser(user);
        }
    }
}