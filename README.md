ENG: Branch "test" of the project contains most of the work done on the assignment. Originally it was meant to be the "playground" of Git for the learning purposes but ended up containing most important work. Deleting it would mean loss of evidence of progress of work on the assignment.

PL: Gałąź "test" projektu zawiera większość pracy nad nim wykonanej. Pierwotnie miała być miejscem testów działania systemu Git lecz okazało się, że zawiera większość wykonanej pracy. Usunięcie tej gałęzi byłoby jednoznaczne z utratą historii postępów wykazanych w projekcie.

