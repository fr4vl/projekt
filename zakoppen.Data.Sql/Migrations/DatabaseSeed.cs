﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata;
using Renci.SshNet;
using zakoppen.Common.Enums;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly zakoppenDbContext _context;

        public DatabaseSeed(zakoppenDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            #region CreateUser
            var UserList = BuildUser();
            _context.User.AddRange(UserList);
            _context.SaveChanges();
            #endregion


            #region CreateTopicForum
            var TopicForumList = BuildTopicForum();
            _context.TopicForum.AddRange(TopicForumList);
            _context.SaveChanges();
            #endregion


            #region CreateTopicSubForum
            var TopicSubForumList = BuildTopicSubForum();
            _context.TopicSubForum.AddRange(TopicSubForumList);
            _context.SaveChanges();
            #endregion


            #region CreatePost
            var Postlist = BuildPost(UserList, TopicSubForumList);
            _context.Post.AddRange(Postlist);
            _context.SaveChanges();
            #endregion

            #region CreateFileHandles
            var FileHandleList = BuildFileHandle(Postlist);
            _context.FileHandle.AddRange(FileHandleList);
            _context.SaveChanges();
            #endregion

            #region CreateSubPost
            var SubPostlist = BuildSubPost(Postlist, UserList);
            _context.SubPost.AddRange(SubPostlist);
            _context.SaveChanges();
            #endregion

            //#region CreateUserCreds
            //var UserCredsList = BuildUserCreds(UserList);
            //_context.UserCreds.AddRange(UserCredsList);
            //_context.SaveChanges();

            //#endregion

        }

        private IEnumerable<FileHandle> BuildFileHandle(IEnumerable<Post> Posts)
        {
            var FileHandleList = new List<FileHandle>();

            for (int i = 1; i <= Posts.Count(); i++)
            {
                var fileHandle = new FileHandle()
                {
                    OriginalFilename = "New001",
                    InternalFilename = "Internal00" + i.ToString(),
                    //ParentId = 0,
                    FileId = i,
                    PostId = i,

                };
                FileHandleList.Add(fileHandle);
            }

            return FileHandleList;
        }

        private IEnumerable<DAO.User> BuildUser()
        {
            var UserList = new List<DAO.User>();

            var user = new DAO.User()
            {
                UserId = "pope2137",
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "PantherBonzai"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "Paptain_Crice"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "IsReal"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "YanushTratch"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "MungeonDuster"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "GandalfSaxGuy10HoursHD"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "NateHiggers"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "DanVarkholme"
            };
            UserList.Add(user);

            user = new DAO.User()
            {
                UserId = "HillyBerrington"
            };
            UserList.Add(user);

            foreach (var us in UserList)
            {
                us.NumericUserId = UserList.Max(u => u.NumericUserId);
            }


            return UserList;
        }

        private IEnumerable<TopicForum> BuildTopicForum()
        {
            var TopicForumList = new List<TopicForum>();

            var topicForum = new TopicForum()
            {
                ForumName = "Cuisine",
                Title = "Cookin\'n\'stuff",
            };
            TopicForumList.Add(topicForum);

            var topicForum2 = new TopicForum()
            {
                ForumName = "Technology",
                Title = "Wires, cables etc. etc.",
            };
            TopicForumList.Add(topicForum2);

            int counter = 1;
            foreach (var forum in TopicForumList)
            {
                forum.ForumId = counter++;
            }

            return TopicForumList;
        }

        
        private IEnumerable<TopicSubForum> BuildTopicSubForum()
        {
            var topicforums = BuildTopicForum();
            var TopicSubForumList = new List<TopicSubForum>();

            var topicSubForum = new TopicSubForum()
            {
                ForumName = "Fishing",
                ParentForumName = "Cuisine",
                Title = "Untitled",
                ImageHref = "NoImg",
            };
            TopicSubForumList.Add(topicSubForum);

            var topicSubForum2 = new TopicSubForum()
            {
                ForumName = "Asian",
                ParentForumName = "Cuisine",
                Title = "Untitled",
                ImageHref = "NoImg",
            };
            TopicSubForumList.Add(topicSubForum2);

            var topicSubForum3 = new TopicSubForum()
            {
                ForumName = "Hacking",
                ParentForumName = "Technology",
                Title = "Untitled",
                ImageHref = "NoImg",
            };
            TopicSubForumList.Add(topicSubForum3);

            var topicSubForum4 = new TopicSubForum()
            {
                ForumName = "Programming",
                ParentForumName = "Technology",
                Title = "Untitled",
                ImageHref = "NoImg",
            };
            TopicSubForumList.Add(topicSubForum4);


            int counter = 1;
            foreach (var forum in TopicSubForumList)
            {
                forum.SubForumId = counter++;
                forum.ParentForumId = (topicforums.First(s => s.ForumName == forum.ParentForumName)).ForumId;
            }

            return TopicSubForumList;
        }


        private IEnumerable<Post> BuildPost(IEnumerable<DAO.User> users, IEnumerable<TopicSubForum> forums)
        {
            var posts = new List<Post>();
            var subfors = BuildTopicSubForum();
            foreach (var forum in forums)
            {
                foreach (var user in users)
                {
                    var newpost = new Post();

                    newpost.TopicSubForumName = forum.ForumName;
                    newpost.UserId = user.UserId;
                    //newpost.Id = (posts.Count() + 1);
                    newpost.Timestamp = DateTime.Now;
                    //newpost.IsOriginalPost = PostOrReply.OriginalPost;
                    newpost.Headline = "Hello! It's me, " + user.UserId.ToString();
                    newpost.Contents = "I am " + user.UserId.ToString() + " and this is my post on "
                                       + forum.ParentForum.ForumName + " in " + forum.ForumName + " category. Post IDs are absolute across entire service.";
                    //newpost.FileId = (posts.Count() + 1);
                    //newpost.ParentSubForumName = forum.ForumName;
                    newpost.ParentSubForumId = (subfors.First(s => s.ForumName == forum.ForumName)).SubForumId;
                    newpost.AuthorUserNumeric = user.NumericUserId;
                    posts.Add(newpost);
                }
            }


            return posts;
        }

        private IEnumerable<SubPost> BuildSubPost(IEnumerable<Post> posts, IEnumerable<DAO.User> Users)
        {
            var SubPostList = new List<SubPost>();

            int counter = 0;
            foreach (var post in posts)
            {
                counter++;
                foreach (var user in Users)
                {
                    SubPost SubPost0 = new SubPost();

                    SubPost0.Timestamp = DateTime.Now;
                    //SubPost0.IsOriginalPost = PostOrReply.Reply;
                    SubPost0.Contents = "This is " + user.UserId + "\'s subpost " + (post.Replies.Count() + (SubPostList.Count() % Users.Count()) + 1).ToString() + " in category " + post.TopicSubForum.ForumName + ". Subposts are locally assigned to each main post.";
                    SubPost0.SubPostId = post.Replies.Count() + (SubPostList.Count() % Users.Count()) + 1;
                    SubPost0.UserId = user.UserId;
                    SubPost0.ReplyingToPostId = counter;
                    SubPost0.SubPostId = SubPostList.Count() + 1;
                    SubPost0.AuthorUserNumeric = user.NumericUserId;
                    SubPostList.Add(SubPost0);
                }
            }


            return SubPostList;
        }


        //private IEnumerable<UserCreds> BuildUserCreds(IEnumerable<DAO.User> users)
        //{
        //    var UserCredsList = new List<UserCreds>();
        //    string defauldDomain = "@mich.au";
        //    foreach (var user in users)
        //    {
        //        UserCreds creds = new UserCreds();
        //        creds.NumericUserId = user.NumericUserId;
        //        creds.user = user;
        //        creds.challenge = user.UserId + defauldDomain;
        //        UserCredsList.Add(creds);
        //    }

        //    return UserCredsList;
        //}
    }
}
