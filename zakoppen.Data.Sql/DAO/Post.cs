﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zakoppen.Common.Enums;

namespace zakoppen.Data.Sql.DAO
{
    public class Post
    {

        public Post()
        {
            Replies = new List<SubPost>();
        }

        public int AuthorUserNumeric { get; set; }

        public int ParentSubForumId { get; set; }
        //public string ParentSubForumName { get; set; }

        public string Headline { get; set; }

        public int PostId { get; set; }
        public DateTime Timestamp { get; set; }
        public string Contents { get; set; }
        // public int? FileId { get; set; }
        //public PostOrReply IsOriginalPost { get; set; }
        public string UserId { get; set; }
        public string TopicSubForumName { get; set; }

        public virtual  TopicSubForum TopicSubForum { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<SubPost> Replies { get; set; }
        public virtual FileHandle FileHandle { get; set; }
    }
}
