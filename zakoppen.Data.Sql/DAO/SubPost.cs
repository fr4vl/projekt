﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zakoppen.Common.Enums;

namespace zakoppen.Data.Sql.DAO
{
    public class SubPost
    {
        public SubPost()
        {

        }

        //public int Id { get; set; }
        public int AuthorUserNumeric { get; set; }
        public DateTime Timestamp { get; set; }
        public string Contents { get; set; }
        //public string AttachedFileUri { get; set; }
        public int ReplyingToPostId { get; set; }
        // public int? FileId { get; set; }
        //public PostOrReply IsOriginalPost { get; set; }
        public string UserId { get; set; }
        public int SubPostId { get; set; } //  ID of post that started the thread; user replies to this post from perspective of database.
        // public int TotalReplyId { get; set; }

                                        
        public virtual Post Post { get; set; }
        public User User { get; set; }
        public virtual FileHandle FileHandle { get; set; }

        //  Scripting will handle users tagging each other in thread. 
        //  From users' perspective when they tag each other it gives impression of threads without
        //  Reddit's depth - perhaps Discord's thread feature (?).
    }
}
