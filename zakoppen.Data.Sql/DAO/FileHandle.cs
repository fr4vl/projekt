﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Data.Sql.DAO
{
    public class FileHandle
    {
        public string OriginalFilename { get; set; }

        public int FileId { get; set; }
        
        public int? PostId { get; set; }
        public int? SubPostId { get; set; }
        public string InternalFilename { get; set; }
        //public int ParentId { get; set; }



        public virtual Post Post { get; set; }
        public virtual SubPost SubPost { get; set; }
    }
}
