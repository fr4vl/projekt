﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Data.Sql.DAO
{
    public  class TopicSubForum
    {

        public TopicSubForum()
        {
            PostsInCategory = new List<Post>();
        }
        public int SubForumId { get; set; }

        public int ParentForumId { get; set; }
        public string ForumName { get; set; }
        public string ParentForumName { get; set; }

        public string Title { get; set; }
        public string ImageHref { get; set; }


        public virtual TopicForum ParentForum { get; set; }
        public virtual ICollection<Post> PostsInCategory { get; set; }
    }
}
