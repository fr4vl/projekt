﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Data.Sql.DAO
{
    public class User
    {
        public string UserId { get; set; }

        public int NumericUserId { get; set; }

        public virtual ICollection<Post> UserPosts { get; set; }
        public virtual ICollection<SubPost> UserReplies { get; set; }

        //public virtual UserCreds Creds { get; set; }
    }

    
}
