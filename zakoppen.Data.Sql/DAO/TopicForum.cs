﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakoppen.Data.Sql.DAO
{
    public class TopicForum
    {
        public TopicForum()
        {
            ForumList = new List<TopicSubForum>();
        }
        public int ForumId { get; set; }
        public string ForumName { get; set; }
        public string Title { get; set; }
        public virtual ICollection<TopicSubForum> ForumList { get; set; }
    }
}
