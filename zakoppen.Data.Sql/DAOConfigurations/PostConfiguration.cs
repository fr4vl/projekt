﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Org.BouncyCastle.Crypto.Parameters;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.DAOConfigurations
{
    public class PostConfiguration: IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post>  builder)
        {
            builder.Property(Post => Post.Headline).IsRequired();
            // builder.Property(Post => Post.Id).IsRequired();
            builder.Property(Post => Post.Timestamp).IsRequired();
            builder.Property(Post => Post.Contents).IsRequired();
//            builder.Property(Post => Post.IsOriginalPost).IsRequired();
            builder.Property(Post => Post.UserId).IsRequired();
            builder.Property(Post => Post.TopicSubForumName).IsRequired();
            
            
            
/*
            // builder.HasKey(Post => Post.Id);

            /*
            builder.HasOne(Post => Post.FileUri)
                .WithOne(FileHandle => FileHandle.Post)
                .HasPrincipalKey<Post>(Post => Post.FileId);

            builder.HasOne(Post => Post.FileUri)
                .WithOne(FileHandle => FileHandle.Post)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey<Post>(FileHandle => FileHandle.FileId);
//            */

            // builder.HasMany(Post => Post.FileUri)
                // .WithOne(FileHandle => FileHandle.Post)
                // .OnDelete(DeleteBehavior.Cascade)
                // .HasPrincipalKey(Post => Post.FileId);
                //
                //*/

            //builder.HasMany(Post => Post.Replies)
            //    .WithOne(SubPost => SubPost.Post)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .HasForeignKey(SubPost => SubPost.ReplyingToPostId);

            //builder.HasMany(x => x.Replies)
            //    .WithOne(x => x.Post)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .HasForeignKey(x => x.AuthorUserNumeric);
            
                
            
            // builder.HasIndex(Post => Post.FileId)
            // .IsUnique();

            builder.ToTable("Post");
        }
    }
}
