﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.DAOConfigurations
{
    public class FileHandleConfiguration: IEntityTypeConfiguration<FileHandle>
    {
        public void Configure(EntityTypeBuilder<FileHandle> builder)
        {
            builder.Property(FileHandle => FileHandle.OriginalFilename).IsRequired();
            builder.Property(FileHandle => FileHandle.InternalFilename).IsRequired();
            //builder.Property(FileHandle => FileHandle.PostId).IsRequired();
            
            //builder.Property(FileHandles => FileHandles.ParentId).IsRequired();
            //builder.Property(FileHandles => FileHandles.LastAccessedTime).IsRequired()
            //builder.Property(FileHandle => FileHandle.FileId).IsRequired();
            //;
            builder.HasKey(FileHandle => FileHandle.FileId);
            
            
            builder.HasIndex(FileHandle => FileHandle.PostId)
                .IsUnique();
            builder.HasIndex(FileHandle => FileHandle.FileId)
                .IsUnique();
            builder.HasIndex(FileHandle => FileHandle.InternalFilename)
                .IsUnique();

            //builder.HasOne(x => x.Post)
            //    .WithOne(x => x.FileHandle)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .HasForeignKey<FileHandle>(x=>x.PostId);

            builder.HasOne(x => x.SubPost)
                .WithOne(x => x.FileHandle)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey<FileHandle>(x=>x.SubPostId);

            //builder.HasOne(x => x.Post)
            //    .WithOne(x => x.FileHandle)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .HasForeignKey<FileHandle>(x => x.PostId);

            builder.HasIndex(u => u.SubPostId).IsUnique();
            builder.HasIndex(u => u.PostId).IsUnique();



            //builder.ToTable("FileHandle");
            builder.ToTable("Media");

        }

    }
}
