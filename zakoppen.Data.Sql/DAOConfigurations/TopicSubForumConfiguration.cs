﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.DAOConfigurations
{
    public class TopicSubForumConfiguration : IEntityTypeConfiguration<TopicSubForum>
    {
        public void Configure(EntityTypeBuilder<TopicSubForum> builder)
        {
            builder.Property(TopicSubForum => TopicSubForum.ForumName).IsRequired();
            builder.Property(TopicSubForum => TopicSubForum.ParentForumName).IsRequired();

            //builder.Property(FileHandles => FileHandles.InternalFilename).IsRequired();
            //builder.Property(FileHandles => FileHandles.ParentId).IsRequired();
            //builder.Property(FileHandles => FileHandles.LastAccessedTime).IsRequired();

            builder.HasKey(TopicSubForum => TopicSubForum.SubForumId);

            builder.HasMany(TopicSubForum => TopicSubForum.PostsInCategory)
                .WithOne(Post => Post.TopicSubForum)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(Post => Post.ParentSubForumId);

            //builder.ToTable("TopicSubForum");
            builder.ToTable("SubForum");
        }
    }
}
