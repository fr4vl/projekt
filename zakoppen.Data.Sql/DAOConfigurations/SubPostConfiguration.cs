﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.DAOConfigurations
{
    public class SubPostConfiguration: IEntityTypeConfiguration<SubPost>
    {
        public void Configure(EntityTypeBuilder<SubPost> builder)
        {
            //builder.Property(Post => Post.Id).IsRequired();
            builder.Property(SubPost => SubPost.Timestamp).IsRequired();
            builder.Property(SubPost => SubPost.Contents).IsRequired();
            //builder.Property(SubPost => SubPost.IsOriginalPost).IsRequired();
            builder.Property(SubPost => SubPost.UserId).IsRequired();
            builder.Property(SubPost => SubPost.SubPostId).IsRequired();
            builder.Property(SubPost => SubPost.ReplyingToPostId).IsRequired();

            // builder.HasKey(SubPost => SubPost.TotalReplyId);
            //builder.HasIndex(SubPost => new { SubPost.SubPostId, SubPost.ReplyingToPostId}).IsUnique();

            //builder.HasOne(SubPost => SubPost.Post)
            //    .WithMany(Post => Post.Replies)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .HasForeignKey(x=>x.Post);


            

            builder.ToTable("SubPost");
        }
    }
}
