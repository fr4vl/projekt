﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.DAOConfigurations
{
    public class UserConfiguration: IEntityTypeConfiguration<DAO.User>
    {
        public void Configure(EntityTypeBuilder<DAO.User> builder)
        {
            builder.Property(User => User.UserId).IsRequired();

            builder.HasMany(User => User.UserPosts)
                .WithOne(Post => Post.User)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(Post => Post.AuthorUserNumeric);


            builder.HasMany(User => User.UserReplies)
                .WithOne(SubPost => SubPost.User)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(SubPost => SubPost.AuthorUserNumeric);

            //builder.HasOne(User => User.Creds)
            //    .WithOne(UserCreds => UserCreds.user)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .HasForeignKey<UserCreds>(UserCreds => UserCreds.NumericUserId);

            builder.HasKey(User => User.NumericUserId);
            builder.ToTable("User");
        }
    }
}
