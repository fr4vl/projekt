﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using zakoppen.Data.Sql.DAO;

namespace zakoppen.Data.Sql.DAOConfigurations
{
    public class TopicForumConfiguraion: IEntityTypeConfiguration<TopicForum>
    {
        public void Configure(EntityTypeBuilder<TopicForum> builder)
        {

            //builder.Property(TopicForum => TopicForum.ForumName).IsRequired();
            builder.HasKey(TopicForum => TopicForum.ForumId);

            builder.HasMany(TopicForum => TopicForum.ForumList)
                .WithOne(TopicSubForum => TopicSubForum.ParentForum)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(TopicSubForum => TopicSubForum.ParentForumId);

            //builder.ToTable("TopicForum");
            builder.ToTable("Forum");

        }
    }
}
