﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using zakoppen.IData.User;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;


namespace zakoppen.Data.Sql.User
{
    
    public class UserRepo : IUserRepo
    {
        private readonly zakoppenDbContext _context;

        public UserRepo(zakoppenDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddUser(Domain.User.User user)
        {
            var UserDAO = new DAO.User
            {
                UserId = user.UserId,
                NumericUserId = user.NumericId
            };
            await _context.AddAsync(UserDAO);
            await _context.SaveChangesAsync();
            return UserDAO.NumericUserId;
        }

        public async Task<Domain.User.User> GetUser(int numericId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.NumericUserId == numericId);

            if (user == null) return new Domain.User.User(-1, null);
            
            return new Domain.User.User(user.NumericUserId,
                user.UserId);
        }

        public async Task<Domain.User.User> GetUser(string userNickname)
        {

            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userNickname);

            if (user == null) return new Domain.User.User(-1, null);

            return new Domain.User.User(user.NumericUserId,
                user.UserId);

        }

        public async Task EditUser(Domain.User.User user)
        {
            var editUser = await _context.User.FirstOrDefaultAsync(x => x.NumericUserId == user.NumericId);
            editUser.UserId = user.UserId;
            await _context.SaveChangesAsync();
        }
    }
}
