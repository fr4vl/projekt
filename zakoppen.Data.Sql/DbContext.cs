﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using zakoppen.Data.Sql.DAO;
using zakoppen.Data.Sql.DAOConfigurations;

namespace zakoppen.Data.Sql
{
    public class zakoppenDbContext : DbContext
    {
        public zakoppenDbContext(DbContextOptions<zakoppenDbContext> options) : base(options) {}

        public virtual DbSet<FileHandle> FileHandle { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<SubPost> SubPost { get; set; }
        public virtual DbSet<TopicForum> TopicForum { get; set; }
        public virtual DbSet<TopicSubForum> TopicSubForum { get; set; }
        public virtual DbSet<DAO.User> User { get; set; }
        //public virtual  DbSet<UserCreds> UserCreds { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new FileHandleConfiguration());
            builder.ApplyConfiguration(new PostConfiguration());
            builder.ApplyConfiguration(new SubPostConfiguration());
            builder.ApplyConfiguration(new TopicForumConfiguraion());
            builder.ApplyConfiguration(new TopicSubForumConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
            //builder.ApplyConfiguration(new UserCredsConfiguration());

        }
    }
}
